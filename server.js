//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + "/build/default"));

app.get('/', (req, res)=> {
  res.status(200).sendFile("index.html", { root: "." });
})

app.listen(port);
console.log("Polymer desde Node on http://localhost:"+ port );
